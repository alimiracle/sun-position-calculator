from setuptools import setup, find_packages

setup(
    name="sun-position-calculator",
    version="1.0.0",
    author="Ali miracle",
    author_email="alimiracle@riseup.net",
    description="A Python library to calculate the sun's position and phases.",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://notabug.org/alimiracle/sun_position_calculator",
    packages=find_packages(),
    install_requires=[],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    keywords="sun position calculator astronomy phases python library",
)